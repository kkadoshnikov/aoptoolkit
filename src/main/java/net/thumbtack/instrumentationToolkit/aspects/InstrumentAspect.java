package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.annotations.Instrument;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
@Aspect
public class InstrumentAspect {

    @Around(Pointcuts.INSTRUMENT_METHOD_POINTCUT)
    public Object byMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Method method = BaseAspect.getMethod(joinPoint);
        Instrument annotation = method.getDeclaredAnnotation(Instrument.class);
        return instrument(joinPoint, method, annotation);
    }

    @Around(Pointcuts.INSTRUMENT_CLASS_POINTCUT)
    public Object byClass(ProceedingJoinPoint joinPoint) throws Throwable {
        Method method = BaseAspect.getMethod(joinPoint);
        Instrument annotation = joinPoint.getTarget().getClass().getDeclaredAnnotation(Instrument.class);
        return instrument(joinPoint, method, annotation);
    }

    public Object instrument(ProceedingJoinPoint joinPoint, Method method, Instrument annotation) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Class instrumentClass = annotation.value();
        net.thumbtack.instrumentationToolkit.interfaces.Instrument instrument =
                (net.thumbtack.instrumentationToolkit.interfaces.Instrument) instrumentClass.newInstance();
        instrument.enter(method, args);
        Object result = joinPoint.proceed();
        instrument.exit(method, result);
        return result;
    }

}
