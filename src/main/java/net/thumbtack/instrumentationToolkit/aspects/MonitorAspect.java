package net.thumbtack.instrumentationToolkit.aspects;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import net.thumbtack.instrumentationToolkit.Aspector;
import net.thumbtack.instrumentationToolkit.annotations.Monitor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
@Aspect
public class MonitorAspect {

    private MetricRegistry registry = Aspector.getMetricRegistry();

    @Around(Pointcuts.MONITOR_METHOD_POINTCUT)
    public Object byMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Monitor annotation = BaseAspect.getMethod(joinPoint).getDeclaredAnnotation(Monitor.class);
        return monitor(joinPoint, annotation);
    }

    @Around(Pointcuts.MONITOR_CLASS_POINTCUT)
    public Object byClass(ProceedingJoinPoint joinPoint) throws Throwable {
        Monitor annotation = joinPoint.getTarget().getClass().getDeclaredAnnotation(Monitor.class);
        return monitor(joinPoint, annotation);
    }

    private Object monitor(ProceedingJoinPoint joinPoint, Monitor annotation) throws Throwable {
        Aspector.MethodMonitor methodMonitor = getMonitor(annotation);
        Timer.Context context = methodMonitor.time();
        Object result = joinPoint.proceed();
        methodMonitor.recordTime(context.stop());
        return result;
    }

    private Aspector.MethodMonitor getMonitor(Monitor annotation) {
        String monitorName = annotation.value();
        if (registry.getTimers().containsKey(monitorName)) {
            return (Aspector.MethodMonitor) registry.getTimers().get(monitorName);
        }
        return registry.register(monitorName, new Aspector.MethodMonitor());
    }

}
