package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.annotations.HandleExceptions;
import net.thumbtack.instrumentationToolkit.annotations.RepeatHandleExceptions;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.lang.annotation.Annotation;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
@Aspect
public class HandleExceptionsAspect {

    @Around(Pointcuts.HANDLE_EXCEPTIONS_METHOD_WITH_ONT_ANNOTATION_POINTCUT)
    public String byMethodWithOneAnnotation(ProceedingJoinPoint joinPoint) throws Throwable {
        HandleExceptions handleExceptions = BaseAspect.getMethod(joinPoint).getDeclaredAnnotation(HandleExceptions.class);
        RepeatHandleExceptions repeatHandleExceptions = createRepeatHandleExceptions(handleExceptions);
        return handleExceptions(joinPoint, repeatHandleExceptions);
    }

    @Around(Pointcuts.HANDLE_EXCEPTIONS_CLASS_WITH_ONE_ANNOTATION_POINTCUT)
    public String byClassWithOneAnnotation(ProceedingJoinPoint joinPoint) throws Throwable {
        HandleExceptions handleExceptions = joinPoint.getTarget().getClass().getDeclaredAnnotation(HandleExceptions.class);
        RepeatHandleExceptions repeatHandleExceptions = createRepeatHandleExceptions(handleExceptions);
        return handleExceptions(joinPoint, repeatHandleExceptions);
    }

    @Around(Pointcuts.HANDLE_EXCEPTIONS_METHOD_WITH_ANNOTATIONS_POINTCUT)
    public String byMethodWithAnnotaions(ProceedingJoinPoint joinPoint) throws Throwable {
        RepeatHandleExceptions repeatHandleExceptions = BaseAspect.getMethod(joinPoint).
                getDeclaredAnnotation(RepeatHandleExceptions.class);
        return handleExceptions(joinPoint, repeatHandleExceptions);
    }

    @Around(Pointcuts.HANDLE_EXCEPTIONS_CLASS_WITH_ANNOTATIONS_POINTCUT)
    public String byClassWithAnnotaions(ProceedingJoinPoint joinPoint) throws Throwable {
        RepeatHandleExceptions repeatHandleExceptions = joinPoint.getTarget().getClass().
                getDeclaredAnnotation(RepeatHandleExceptions.class);
        return handleExceptions(joinPoint, repeatHandleExceptions);
    }

    private String handleExceptions(ProceedingJoinPoint joinPoint, RepeatHandleExceptions repeatHandleExceptions) throws Throwable {
        Object result;
        try {
            result = joinPoint.proceed();
        } catch (Throwable error) {
            return getResult(repeatHandleExceptions, error);
        }
        return result.toString();
    }

    private String getResult(RepeatHandleExceptions repeatHandleExceptions, Throwable error) throws Throwable {
        for (HandleExceptions handleExceptions : repeatHandleExceptions.value()) {
            for (Class type : handleExceptions.types()) {
                if (type.equals(error.getClass())) {
                    return handleExceptions.ret();
                }
            }
        }
        throw error;
    }

    public RepeatHandleExceptions createRepeatHandleExceptions(final HandleExceptions handleExceptions) {
        return new RepeatHandleExceptions() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return null;
            }

            @Override
            public HandleExceptions[] value() {
                return new HandleExceptions[]{handleExceptions};
            }
        };
    }

}
