package net.thumbtack.instrumentationToolkit.aspects;

/**
 * Created by Kirill on 15.08.2015.
 */
public class Pointcuts {
    public static final String HANDLE_EXCEPTIONS_METHOD_WITH_ONT_ANNOTATION_POINTCUT =
            "execution(@net.thumbtack.instrumentationToolkit.annotations.HandleExceptions * *(..))";
    public static final String HANDLE_EXCEPTIONS_CLASS_WITH_ONE_ANNOTATION_POINTCUT =
            "execution(* @net.thumbtack.instrumentationToolkit.annotations.HandleExceptions *.*(..)) \n" +
            "             && !execution(@net.thumbtack.instrumentationToolkit.annotations.HandleExceptions * *.*(..))";
    public static final String HANDLE_EXCEPTIONS_METHOD_WITH_ANNOTATIONS_POINTCUT =
            "execution(@net.thumbtack.instrumentationToolkit.annotations.RepeatHandleExceptions * *(..))";
    public static final String HANDLE_EXCEPTIONS_CLASS_WITH_ANNOTATIONS_POINTCUT =
            "execution(* @net.thumbtack.instrumentationToolkit.annotations.RepeatHandleExceptions *.*(..)) \n" +
            "             && !execution(@net.thumbtack.instrumentationToolkit.annotations.RepeatHandleExceptions * *.*(..))";
    public static final String INSTRUMENT_METHOD_POINTCUT =
            "execution(@net.thumbtack.instrumentationToolkit.annotations.Instrument * *(..))";
    public static final String INSTRUMENT_CLASS_POINTCUT =
            "execution(* @net.thumbtack.instrumentationToolkit.annotations.Instrument *.*(..)) \n" +
            "             && !execution(@net.thumbtack.instrumentationToolkit.annotations.Instrument * *.*(..))";
    public static final String LOGGING_METHOD_POINTCUT =
            "execution(@net.thumbtack.instrumentationToolkit.annotations.Log * *(..))";
    public static final String LOGGING_CLASS_POINTCUT =
            "execution(* @net.thumbtack.instrumentationToolkit.annotations.Log *.*(..)) \n" +
            "             && !execution(@net.thumbtack.instrumentationToolkit.annotations.Log * *.*(..))";
    public static final String MONITOR_CLASS_POINTCUT =
            "execution(* @net.thumbtack.instrumentationToolkit.annotations.Monitor *.*(..)) \n" +
            "             && !execution(@net.thumbtack.instrumentationToolkit.annotations.Monitor * *.*(..))";
    public static final String MONITOR_METHOD_POINTCUT =
            "execution(@net.thumbtack.instrumentationToolkit.annotations.Monitor * *(..))";
}
