package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.annotations.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;

import java.lang.reflect.Modifier;

@Aspect
public class LoggingAspect {

    private Logger logger;

    @Around(Pointcuts.LOGGING_METHOD_POINTCUT)
    public Object byMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Log log = BaseAspect.getMethod(joinPoint).getDeclaredAnnotation(Log.class);
        return log(joinPoint, log);
    }

    @Around(Pointcuts.LOGGING_CLASS_POINTCUT)
    public Object byClass(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Log annotation = joinPoint.getTarget().getClass().getDeclaredAnnotation(Log.class);
        if (!annotation.onlyPublic() || Modifier.isPublic(methodSignature.getModifiers())) {
            return log(joinPoint, annotation);
        }
        return joinPoint.proceed();
    }

    private Object log(ProceedingJoinPoint joinPoint, Log annotation) throws Throwable {
        setLogger(annotation);
        StringBuilder method = readMethod((MethodSignature) joinPoint.getSignature());
        if (annotation.message().equals("")) {
            return logDefault(joinPoint, annotation, method);
        } else {
            return logCustom(joinPoint, annotation, method);
        }
    }

    private Object logDefault(ProceedingJoinPoint joinPoint, Log annotation, StringBuilder method) throws Throwable {
        StringBuilder message = new StringBuilder(method);
        if (annotation.params()) {
            message.append(" ").append(readParams(joinPoint));
        }
        if (annotation.inout()) {
            logger.info("before " + message);
        }
        Object result = joinPoint.proceed();
        message.append(" is invoked");
        if (annotation.timestamp()) {
            message.append(" on ").append(System.currentTimeMillis());
        }
        logger.info(message.toString());
        return result;
    }

    private Object logCustom(ProceedingJoinPoint joinPoint, Log annotation, StringBuilder method) throws Throwable {
        String message = annotation.message();
        if (annotation.inout()) {
            logCustomMessage(joinPoint, annotation, method, "before " + message);
        }
        Object result = joinPoint.proceed();
        logCustomMessage(joinPoint, annotation, method, message);
        return result;
    }

    private void logCustomMessage(ProceedingJoinPoint joinPoint, Log annotation, StringBuilder method, String message) {
        if (annotation.timestamp() && annotation.params()) {
            if (annotation.params()) {
                readParams(joinPoint);
                logger.info(message, method, readParams(joinPoint), System.currentTimeMillis());
            } else {
                logger.info(message, method, System.currentTimeMillis());
            }
        } else {
            if (annotation.params()) {
                readParams(joinPoint);
                logger.info(message, method, readParams(joinPoint));
            } else {
                logger.info(message, method);
            }
        }
    }

    private void setLogger(Log log) {
        if (log.logger().equals("")) {
            logger = org.slf4j.LoggerFactory.getLogger("sout");
        } else {
            logger = org.slf4j.LoggerFactory.getLogger(log.logger());
        }
    }

    private StringBuilder readMethod(MethodSignature methodSignature) {
        StringBuilder method = new StringBuilder(methodSignature.getDeclaringType().getSimpleName());
        method.append(".").append(methodSignature.getName());
        return method;
    }

    private String readParams(ProceedingJoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        StringBuilder result = new StringBuilder("{");
        for (Object obj : args) {
            result.append(obj.getClass().getSimpleName()).append(": \"");
            result.append(obj).append("\", ");
        }
        if (args.length > 0) {
            result.deleteCharAt(result.length() - 1);
            result.deleteCharAt(result.length() - 1);
        }
        result.append("}");
        return result.toString();
    }

}
