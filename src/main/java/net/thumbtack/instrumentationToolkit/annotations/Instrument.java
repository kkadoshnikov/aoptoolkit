package net.thumbtack.instrumentationToolkit.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Instrument {
    Class<? extends net.thumbtack.instrumentationToolkit.interfaces.Instrument> value();
}
