package net.thumbtack.instrumentationToolkit.annotations;

import java.lang.annotation.*;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
@Repeatable(RepeatHandleExceptions.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface HandleExceptions {
    Class<? extends Exception>[] types();

    String ret();
}
