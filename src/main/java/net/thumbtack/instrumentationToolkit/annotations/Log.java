package net.thumbtack.instrumentationToolkit.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by kkadoshnikov on 8/11/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Log {
    boolean params() default false;

    boolean inout() default false;

    boolean onlyPublic() default true;

    boolean timestamp() default false;

    String message() default "";

    String logger() default "sout";
}
