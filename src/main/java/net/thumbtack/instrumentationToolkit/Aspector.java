package net.thumbtack.instrumentationToolkit;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import net.thumbtack.instrumentationToolkit.interfaces.Monitor;

/**
 * Created by kkadoshnikov on 8/13/15.
 */

public class Aspector {

    private static MetricRegistry registry = new MetricRegistry();

    public static class MethodMonitor extends Timer implements Monitor {

        private long total;
        private long minimum = Long.MAX_VALUE;
        private long maximum;

        public long getTotal() {
            return total;
        }

        public long getMinimum() {
            return minimum;
        }

        public long getMaximum() {
            return maximum;
        }

        public long getAverage() {
            return total / getCount();
        }

        public void recordTime(long time) {
            total += time;
            if (time > maximum) {
                maximum = time;
            }
            if (time < minimum) {
                minimum = time;
            }
        }

    }

    public static MetricRegistry getMetricRegistry() {
        return registry;
    }

    public static void setMetricRegistry(MetricRegistry registry) {
        Aspector.registry = registry;
    }

    public static Monitor getMonitor(String name) {
        return (Monitor) registry.getTimers().get(name);
    }

}
