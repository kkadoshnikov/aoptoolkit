package net.thumbtack.instrumentationToolkit.interfaces;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public interface Monitor {

    long getTotal();

    long getMinimum();

    long getMaximum();

    long getAverage();

    long getCount();

}
