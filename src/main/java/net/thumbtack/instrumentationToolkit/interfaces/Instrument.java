package net.thumbtack.instrumentationToolkit.interfaces;

import java.lang.reflect.Method;

/**
 * Created by kkadoshnikov on 8/12/15.
 */
public interface Instrument {
    boolean enter(Method method, Object... arguments);

    void exit(Method method, Object returnedValue);
}
