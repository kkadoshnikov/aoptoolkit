package net.thumbtack.instrumentationToolkit.testClasses;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
@net.thumbtack.instrumentationToolkit.annotations.Instrument(TestInstrumentImpl.class)
public class InstrumentTest {

    @net.thumbtack.instrumentationToolkit.annotations.Instrument(TestInstrumentImpl.class)
    public String testInstrument(Integer i, String s) {
        return "result";
    }

    public String nonAnnotatedMethod(Integer i, String s) {
        return "result";
    }

}
