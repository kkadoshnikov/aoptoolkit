package net.thumbtack.instrumentationToolkit.testClasses;

import net.thumbtack.instrumentationToolkit.annotations.Log;

import java.io.IOException;

/**
 * Created by kkadoshnikov on 8/11/15.
 */
@Log(onlyPublic = false)
public class LogTest {

    @Log(params = true)
    public String logDefault(String hello) {
        return hello;
    }

    @Log(params = true, message = "1: {} 2: {} ")
    public String logCustom(String hello) {
        return hello;
    }

    public void nonAnnotatedMethod() {
    }

    @Log(inout = true)
    public void logInout() {
    }

    @Log(logger = "customLog")
    public void customLogger() {
    }

    public void callOtherMethod() {
        otherMethod();
    }

    private void otherMethod() {

    }

}
