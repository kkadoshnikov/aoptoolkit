package net.thumbtack.instrumentationToolkit.testClasses;

import net.thumbtack.instrumentationToolkit.annotations.Monitor;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
@Monitor("nonAnnotated")
public class MonitorTest {

    @Monitor("test")
    public void method() {

    }

    public void nonAnnotatedMethod() {

    }

}
