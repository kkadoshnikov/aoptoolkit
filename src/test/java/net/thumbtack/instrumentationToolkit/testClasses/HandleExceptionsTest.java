package net.thumbtack.instrumentationToolkit.testClasses;

import net.thumbtack.instrumentationToolkit.annotations.HandleExceptions;

import java.io.IOException;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
@HandleExceptions(types = {IOException.class}, ret = "result")
public class HandleExceptionsTest {

    @HandleExceptions(types = {IOException.class}, ret = "result")
    public String methodThrowIOException() throws IOException {
        throw new IOException("bad");
    }

    @HandleExceptions(types = {ClassCastException.class}, ret = "result1")
    @HandleExceptions(types = {IOException.class, IllegalArgumentException.class}, ret = "result2")
    public String methodThrowCustomException(Exception e) throws Exception {
        throw e;
    }

    public String nonAnnotatedMethod() throws IOException {
        throw new IOException("bad");
    }

}
