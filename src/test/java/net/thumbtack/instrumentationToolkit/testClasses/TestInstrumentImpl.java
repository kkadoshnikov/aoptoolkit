package net.thumbtack.instrumentationToolkit.testClasses;

import net.thumbtack.instrumentationToolkit.interfaces.Instrument;

import java.lang.reflect.Method;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public class TestInstrumentImpl implements Instrument {

    private static TestInstrumentImpl instance;

    private Method enterMethod;
    private Object[] arguments;
    private Method exitMethod;
    private Object returnedValue;

    @Override
    public boolean enter(Method method, Object... arguments) {
        enterMethod = method;
        this.arguments = arguments;
        return false;
    }

    @Override
    public void exit(Method method, Object returnedValue) {
        exitMethod = method;
        this.returnedValue = returnedValue;
        instance = this;
    }

    public Method getEnterMethod() {
        return enterMethod;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public Method getExitMethod() {
        return exitMethod;
    }

    public Object getReturnedValue() {
        return returnedValue;
    }

    public static TestInstrumentImpl getInstance() {
        return instance;
    }
}

