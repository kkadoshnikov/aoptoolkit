package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.testClasses.InstrumentTest;
import net.thumbtack.instrumentationToolkit.testClasses.TestInstrumentImpl;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public class InstrumentAspectTest {

    private static InstrumentTest instrumentTest;

    @BeforeClass
    public static void init() throws Exception {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config_ltw.xml");
        instrumentTest = (InstrumentTest) appContext.getBean("instrumentTest");
    }

    @Test
    public void testAnnotatedMethod() {
        instrumentTest.testInstrument(3, "string");
        testInstrument("testInstrument");
    }

    @Test
    public void testAnnotatedClass() {
        instrumentTest.nonAnnotatedMethod(3, "string");
        testInstrument("nonAnnotatedMethod");
    }

    private void testInstrument(String methodName) {
        TestInstrumentImpl testInstrument = TestInstrumentImpl.getInstance();
        Assert.assertEquals("result", testInstrument.getReturnedValue());
        Object[] args = {3, "string"};
        Assert.assertArrayEquals(args, testInstrument.getArguments());
        Assert.assertEquals(methodName, testInstrument.getEnterMethod().getName());
        Assert.assertEquals(methodName, testInstrument.getExitMethod().getName());
    }

}
