package net.thumbtack.instrumentationToolkit.aspects;

import com.codahale.metrics.Timer;
import net.thumbtack.instrumentationToolkit.Aspector;
import net.thumbtack.instrumentationToolkit.testClasses.LogTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.util.Scanner;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public class LoggingAspectTest {

    private static LogTest logTest;

    private static ByteArrayOutputStream outputStream;

    @BeforeClass
    public static void init() throws Exception {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config_ltw.xml");
        logTest = (LogTest) appContext.getBean("logTest");
        outputStream = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(outputStream, true);
        System.setOut(out);
    }

    @Test
    public void testLogDefault() {
        logTest.logDefault("hello");
        Assert.assertEquals("default LogTest.logDefault {String: \"hello\"} is invoked", getLastString());
    }

    @Test
    public void testLogCustom() throws IOException {
//        System.out.println("before custom message");
        logTest.logCustom("hello");
        Assert.assertEquals("default 1: LogTest.logCustom 2: {String: \"hello\"} ", getLastString());
    }

    @Test
    public void testLogAnnotatedClass() {
        logTest.nonAnnotatedMethod();
        Assert.assertEquals("default LogTest.nonAnnotatedMethod is invoked", getLastString());
    }

    @Test
    public void testInoutLog() {
        logTest.logInout();
        String[] expected = {"default before LogTest.logInout",
                "default LogTest.logInout is invoked"};
        Assert.assertArrayEquals(expected, getTwoLastString());
    }

    @Test
    public void testCustomLogger() {
        logTest.customLogger();
        Assert.assertEquals("custom LogTest.customLogger is invoked", getLastString());
    }

    @Test
    public void testCallOtherMethod() {
        logTest.callOtherMethod();
        String[] expected = {"default LogTest.otherMethod is invoked",
                "default LogTest.callOtherMethod is invoked"};
        Assert.assertArrayEquals(expected, getTwoLastString());
    }

    private String getLastString() {
        Scanner in = new Scanner(new BufferedInputStream(new ByteArrayInputStream(outputStream.toByteArray())));
        String s = null;
        while (in.hasNext()) {
            s = in.nextLine();
        }
        return s;
    }

    private String[] getTwoLastString() {
        Scanner in = new Scanner(new BufferedInputStream(new ByteArrayInputStream(outputStream.toByteArray())));
        String[] s = new String[2];
        while (in.hasNext()) {
            s[0] = s[1];
            s[1] = in.nextLine();
        }
        return s;
    }

}