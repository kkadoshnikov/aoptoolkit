package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.testClasses.HandleExceptionsTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.EOFException;
import java.io.IOException;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public class HandleExceptionsAspectTest {

    private static HandleExceptionsTest handleExceptionsTest;

    @BeforeClass
    public static void init() throws Exception {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config_ltw.xml");
        handleExceptionsTest = (HandleExceptionsTest) appContext.getBean("handleExceptionsTest");
    }

    @Test
    public void testHandleIOException() throws IOException {
        Assert.assertEquals("result", handleExceptionsTest.methodThrowIOException());
    }

    @Test
    public void testAnnotatedClass() throws IOException {
        Assert.assertEquals("result", handleExceptionsTest.nonAnnotatedMethod());
    }

    @Test(expected = EOFException.class)
    public void testHandleCustomException() throws Exception {
        Assert.assertEquals("result2", handleExceptionsTest.methodThrowCustomException(new IOException("a")));
        Assert.assertEquals("result1", handleExceptionsTest.methodThrowCustomException(new ClassCastException("a")));
        Assert.assertEquals("result2", handleExceptionsTest.methodThrowCustomException(new IllegalArgumentException("a")));
        handleExceptionsTest.methodThrowCustomException(new EOFException("a"));
    }

}
