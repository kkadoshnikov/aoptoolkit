package net.thumbtack.instrumentationToolkit.aspects;

import com.codahale.metrics.Timer;
import net.thumbtack.instrumentationToolkit.Aspector;
import net.thumbtack.instrumentationToolkit.testClasses.LogTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

/**
 * Created by kkadoshnikov on 8/17/15.
 */
public class SpeedTest {

    private static Logger logger = org.slf4j.LoggerFactory.getLogger("sout");

    private static PrintStream printStream = System.out;

    @BeforeClass
    public static void init() throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(outputStream, true);
        System.setOut(out);
    }

    @Test
    public void testDefault() {
        System.out.println("default");
        testSpeed(new Aspector.MethodMonitor(), SpeedTest::logMethod);
    }

    @Test
    public void testSpeedSpringProxy() {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config.xml");
        LogTest logTest = (LogTest) appContext.getBean("logTest");
        printStream.println("spring proxy");
        testSpeed(new Aspector.MethodMonitor(), logTest::logDefault);
    }

    @Test
    public void testSpeedLTW() {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config_ltw.xml");
        LogTest logTest = (LogTest) appContext.getBean("logTest");
        System.out.println("LTW");
        testSpeed(new Aspector.MethodMonitor(), logTest::logDefault);
    }

    @Test
    public void testSpeedCTW() {
        printStream.println("CTW");
        LogTest logTest = new LogTest();
        testSpeed(new Aspector.MethodMonitor(), logTest::logDefault);
    }

    private void testSpeed(Aspector.MethodMonitor monitor, Consumer<String> consumer) {
        for (int i = 0; i < 1000000; i++) {
            Timer.Context context = monitor.time();
            consumer.accept("hello");
//            test.logDefault("hello");
            monitor.recordTime(context.stop());
        }
        printStream.println("minimum " + monitor.getMinimum());
        printStream.println("average " + monitor.getAverage());
        printStream.println("maximum " + monitor.getMaximum());
        printStream.println("total " + monitor.getTotal());
    }

    private static void logMethod(String message) {
        loggedMethod("hello");
        logger.info("Aspector.Log: LogTest.callMethod {} is invoked", "hello");
    }

    private static String loggedMethod(String hello) {
        return hello;
    }

}
