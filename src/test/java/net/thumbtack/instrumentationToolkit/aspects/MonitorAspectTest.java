package net.thumbtack.instrumentationToolkit.aspects;

import net.thumbtack.instrumentationToolkit.Aspector;
import net.thumbtack.instrumentationToolkit.interfaces.Monitor;
import net.thumbtack.instrumentationToolkit.testClasses.MonitorTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by kkadoshnikov on 8/13/15.
 */
public class MonitorAspectTest {

    private static MonitorTest monitorTest;

    @BeforeClass
    public static void init() throws Exception {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("test_config_ltw.xml");
        monitorTest = (MonitorTest) appContext.getBean("monitorTest");
    }

    @Test
    public void testAnnotatedMethod() {
        for (int i = 0; i < 10; i++) {
            monitorTest.method();
        }
        Monitor monitor = Aspector.getMonitor("test");
        testMonitor(monitor);
    }

    @Test
    public void testAnnotatedClass() {
        for (int i = 0; i < 10; i++) {
            monitorTest.nonAnnotatedMethod();
        }
        Monitor monitor = Aspector.getMonitor("nonAnnotated");
        testMonitor(monitor);
    }

    private void testMonitor(Monitor monitor) {
        Assert.assertEquals(10, monitor.getCount());
        Assert.assertTrue(monitor.getMinimum() <= monitor.getAverage());
        Assert.assertTrue(monitor.getAverage() <= monitor.getMaximum());
        Assert.assertTrue(monitor.getMaximum() <= monitor.getTotal());
        Assert.assertEquals(monitor.getAverage(), monitor.getTotal() / monitor.getCount());
    }

}
